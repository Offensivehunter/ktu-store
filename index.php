
<?php
  session_start();
  $count = 0;
  // connecto database
  
  $title = "Index";
 
  require_once "./functions/database_functions.php";
  $conn = db_connect();
  $row = select4LatestBook($conn);
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
     <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <title>HTML Education Template</title>

    <!-- Google font -->
    <link href="https://fonts.googleapis.com/css?family=Lato:700%7CMontserrat:400,600" rel="stylesheet">

    <!-- Bootstrap -->
    <link type="text/css" rel="stylesheet" href="css/bootstrap.min.css"/>

    <!-- Font Awesome Icon -->
    <link rel="stylesheet" href="css/font-awesome.min.css">

    <!-- Custom stlylesheet -->
    <link type="text/css" rel="stylesheet" href="css/style.css"/>
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">


    

    </head>
  <body>
    

    <!-- Header -->

      <div class="container">

        <!-- row -->
        <!-- Header -->
    <header id="header" >
      <div class="container">

        <div class="navbar-header">
          <!-- Logo -->
          <div class="navbar-brand">
            <a  href="index.html">
              <img src="pic.png" alt="logo" width="150px" height="90px">
            </a>
          </div>
          <!-- /Logo -->

          <!-- Mobile toggle -->
          <button class="navbar-toggle">
            <span></span>
          </button>
          <!-- /Mobile toggle -->
        </div>

        <!-- Navigation -->
        <nav id="nav" >
          <ul class="main-menu nav navbar-nav navbar-right" style="position: relative;top: 18px;">
            <li><a href="index.html">Home</a></li>
            <li><a href="#">Notice board</a></li>
            <li><a href="#">KTU Rules</a></li>
            <li><a href="blog.html">Results</a></li>
            <li><a href="contact.html">Contact us</a></li>
            <li><a href="contact.html">Upload</a></li>
          </ul>
        </nav>
        <!-- /Navigation -->

      </div>
    </header>
    <br><br><br>
    <a class="main-button icon-button" href="#" style="width: 100%;"><span style="position: relative; left: 40%; ">KTU BOOKS RESALE</span></a>
    <br><br>
        <div id="courses-wrapper">

          <!-- row -->
          <div class="row">

            <!-- single course -->
            <div class="col-md-3 col-sm-3 col-xs-6">
              <div class="course">
                <a href="semcs.html" class="course-img">
                  <img src="./img/course03.jpg" alt=""height="140px">
                  <i class="course-link-icon fa fa-arrow-right"></i>
                </a>
                <a class="course-title" href="semcs.html">COMPUTER SCIENCE</a>
                
              </div>
            </div>
            <!-- /single course -->

            <!-- single course -->
            <div class="col-md-3 col-sm-3 col-xs-6">
              <div class="course">
                <a href="semeee.html" class="course-img">
                  <img src="./img/course04.jpg" alt="" height="140px" >
                  <i class="course-link-icon fa fa-arrow-right"></i>
                </a>
                <a class="course-title" href="semeee.html">ELECTRICAL ENGINEERING</a>
                
              </div>
            </div>
            <!-- /single course -->

            <!-- single course -->
            <div class="col-md-3 col-sm-3 col-xs-6">
              <div class="course">
                <a href="semce.html" class="course-img">
                  <img src="./img/course01.jpg" alt=""height="140px">
                  <i class="course-link-icon fa fa-arrow-right"></i>
                </a>
                <a class="course-title" href="semce.html">CIVIL ENGINEERING</a>
                
              </div>
            </div>
            <div class="col-md-3 col-sm-3 col-xs-6">
              <div class="course">
                <a href="semec.html" class="course-img">
                  <img src="./img/course02.jpg" alt=""height="140px">
                  <i class="course-link-icon fa fa-arrow-right"></i>
                </a>
                <a class="course-title" href="semec.html">ELECTRONICS AND COMMUNICATION</a>
                
              </div>
            </div>

            
            
            

          </div>
          <!-- /row -->

          <!-- row -->
          <div class="row">

            <!-- single course -->
            <div class="col-md-3 col-sm-3 col-xs-6">
              <div class="course">
                <a href="semme.html" class="course-img">
                  <img src="./img/course05.jpg" alt="" height="140px">
                  <i class="course-link-icon fa fa-arrow-right"></i>
                </a>
                <a class="course-title" href="semme.html">MECHANICAL ENGINEERING</a>
                
              </div>
            </div>
            <!-- /single course -->

            <!-- single course -->
            <div class="col-md-3 col-sm-3 col-xs-6">
              <div class="course">
                <a href="#" class="course-img">
                  <img src="./img/course06.jpg" alt="" height="140px">
                  <i class="course-link-icon fa fa-arrow-right"></i>
                </a>
                <a class="course-title" href="#">CHEMICAL ENGINEERING</a>
                
              </div>
            </div>
            <!-- /single course -->

            <!-- single course -->
            <div class="col-md-3 col-sm-3 col-xs-6" height="140px">
              <div class="course">
                <a href="#" class="course-img">
                  <img src="./img/course07.jpg" alt="" height="140px">
                  <i class="course-link-icon fa fa-arrow-right"></i>
                </a>
                <a class="course-title" href="#">AERONAUTICAL ENGINEERING</a>
                
              </div>
            </div>
            <!-- /single course -->


            <!-- single course -->
            <div class="col-md-3 col-sm-3 col-xs-6">
              <div class="course">
                <a href="sem.html" class="course-img">
                  <img src="./img/course08.jpg" alt="" height="140px">
                  <i class="course-link-icon fa fa-arrow-right"></i>
                </a>
                <a class="course-title" href="#">MORE COURSES</a>
              </div>
            </div>
            

          </div>
          

        </div>
        

        
      </div>
      <!-- container -->

    </div>
    <!-- /Courses -->

            <div class="container">
      <div class="row">
                <div class="col-md-12">
                  <div style="background-color: #FF771A; height: 90px; margin: 0; padding:0;">
                    <h3 style="color: white; position: relative;top: 30px; left: 10px;">LATEST UPDATES</h3>
                  </div>
                </div>
                <br>
        <?php foreach($row as $book) { ?>
      	<div class="col-md-12">
          <div class="col-md-2">
            <hr>
      		<a href="book.php?bookisbn=<?php echo $book['book_isbn']; ?>">
           <img class="img-th" width="80px;" height="70px;" src="./bootstrap/img/<?php echo $book['book_image']; ?>">
           </div>
           <div class="col-md-10">
           <span class="text-concat text-center text-muted" style="position: relative;top: 30px; line-height:20px;"><?php echo $book['book_descr']; ?> </span>
           <br>
           </div>
           
          </a>

      	</div>  
     

          <?php } ?>
         
      
</div>
</div></div>
      

      
<?php
  if(isset($conn)) {mysqli_close($conn);}
  
?>
<br><br>

<div class="container">

        <!-- row -->
        <div class="row">
          <div class="section-header text-center">
            <h2>KTU RULES</h2>
            
          </div>
        </div>
        <hr><br>
        
     



      <div class="row app-marketing-grid">
      <div class="col-sm-3 p-x-md m-b-lg">
        <img class="m-b" src="img/startup-9.svg">
        <p><strong>24/7 support.</strong> We’re always here for you no matter what time of day.</p>
      </div>
      <div class="col-sm-3 p-x-md m-b-lg">
        <img class="m-b" src="img/startup-10.svg">
        <p><strong>E-commerce.</strong> We automatically handle all sales analytics.</p>
      </div>
      <div class="col-sm-3 p-x-md m-b-lg">
        <img class="m-b" src="img/startup-11.svg">
        <p><strong>Turnaround.</strong> Our data analysis is distributed, so it processes in seconds.</p>
      </div>
      <div class="col-sm-3 p-x-md m-b-lg">
        <img class="m-b" src="img/startup-11.svg">
        <p><strong>Turnaround.</strong> Our data analysis is distributed, so it processes in seconds.</p>
      </div>
    </div>

    <div class="row app-marketing-grid">
      <div class="col-sm-3 p-x-md m-b-lg">
        <img class="m-b" src="img/startup-12.svg">
        <p><strong>Rich calculations.</strong> Limitless ways to splice and dice your data.</p>
      </div>
      <div class="col-sm-3 p-x-md m-b-lg">
        <img class="m-b" src="img/startup-13.svg">
        <p><strong>Mobile apps.</strong> iOS and Android apps available for monitoring.</p>
      </div>
      <div class="col-sm-3 p-x-md m-b-lg">
        <img class="m-b" src="img/startup-14.svg">
        <p><strong>Secure connections.</strong> Every single request is routed through HTTPS.</p>
      </div>
      <div class="col-sm-3 p-x-md m-b-lg">
        <img class="m-b" src="img/startup-14.svg">
        <p><strong>Secure connections.</strong> Every single request is routed through HTTPS.</p>
      </div>

    </div>
  </div>
</div>
<footer id="footer" class="section">

      <!-- container -->
      <div class="container">

        <!-- row -->
        <div class="row">

          <!-- footer logo -->
          <div class="col-md-6">
            <div class="footer-logo">
              <a class="logo" href="index.html">
                <p>KTU STORE</p>
              </a>
            </div>
          </div>
          <!-- footer logo -->

          <!-- footer nav -->
          <div class="col-md-6">
            <ul class="footer-nav">
              <li><a href="index.html">Home</a></li>
              <li><a href="#">About</a></li>
              <li><a href="#">Courses</a></li>
              <li><a href="blog.html">Blog</a></li>
              <li><a href="contact.html">Contact</a></li>
            </ul>
          </div>
          <!-- /footer nav -->

        </div>
        <!-- /row -->

        <!-- row -->
        <div id="bottom-footer" class="row">

          <!-- social -->
          <div class="col-md-4 col-md-push-8">
            <ul class="footer-social">
              <li><a href="#" class="facebook"><i class="fa fa-facebook"></i></a></li>
              <li><a href="#" class="twitter"><i class="fa fa-twitter"></i></a></li>
              <li><a href="#" class="google-plus"><i class="fa fa-google-plus"></i></a></li>
              <li><a href="#" class="instagram"><i class="fa fa-instagram"></i></a></li>
              <li><a href="#" class="youtube"><i class="fa fa-youtube"></i></a></li>
              <li><a href="#" class="linkedin"><i class="fa fa-linkedin"></i></a></li>
            </ul>
          </div>
          
            



          </section>
          
          <div class="col-md-8 col-md-pull-4">
            <div class="footer-copyright">
              <span>&copy; Copyright 2018. All Rights Reserved. | Developed by <a href="http://arunabeyaantrix.github.io">ARUN</a> | Template by <a href="https://colorlib.com">Colorlib</a></span>
            </div>
          </div>
          

        </div>
        

      </div>
      

    </footer>
    

    
    <div id='preloader'><div class='preloader'></div></div>
    


    <!-- jQuery Plugins -->
    <script type="text/javascript" src="js/jquery.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/main.js"></script>

  </body>
</html>
